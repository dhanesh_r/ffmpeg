#!/bin/bash
pid_file='/var/run/ch01.pid'
pid=`sudo cat $pid_file`
check=`ps aux | grep [f]fmpeg`

if [ -z "$check" ];
then
        ffmpeg -nostats -loglevel 0 -rtsp_transport udp -i 'rtsp://192.168.2.145:8557/PSIA/Streaming/channels/2?videoCodecType=H.264' \
 -c:v copy -c:a aac -b:a 64k -ac 1 -f flv 'rtmp://104.207.134.248/cams/streamcam' &
    ch_pid=$!
    echo $ch_pid
    sudo echo $ch_pid > $pid_file
    sudo crontab /home/pi/fb_ctrl/ffmpeg/mycron
else
    "Process running"
fi

node_check=`pgrep node`
if [ -z "$node_check" ]; then
    sudo service nodeserver start
else
    echo "Node Running"
fi

