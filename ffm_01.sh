#!/bin/sh
pid_file="/var/run/ch01.pid"
name="ch_01"


case "$1" in
 start)
    echo "ch_01 ffmpeg starts";
	#ffmpeg -rtsp_transport udp -i "rtsp://192.168.2.145:8557/PSIA/Streaming/channels/2?videoCodecType=H.264" 
	#-c:v copy -c:a aac -b:a 64k -ac 1 -f flv rtmp://104.207.134.248/cams/streamcam
    ffmpeg -nostats -loglevel 0 -rtsp_transport udp -i 'rtsp://192.168.2.145:8557/PSIA/Streaming/channels/2?videoCodecType=H.264' \
 -c:v copy -c:a aac -b:a 64k -ac 1 -f flv 'rtmp://104.207.134.248/cams/streamcam' &

        ch_pid=$!
    sudo crontab /home/pi/fb_ctrl/ffmpeg/mycron
    echo $ch_pid
    echo $ch_pid > $pid_file
    echo `( >>/dev/null )&`;
         ;;
 stop)
    echo "ch_01 ffmpeg stops";
    PID=`cat $pid_file 2>/dev/null`
    sudo crontab -r 
    sudo pkill -f ffmpeg
    pid_f=`pgrep ffmpeg`
    sudo kill -9 $pid_f >/dev/null 2>&1
    sudo kill $PID >/dev/null 2>&1
         ;;
        *)
    echo "Usage: /etc/init.d/$name {start|stop}"
         exit 1
         ;;
 esac


exit 0
